package com.CoderXAmod.Electronic.LoginWithGoogleAPI;

import com.CoderXAmod.Electronic.Entities.User;
import com.CoderXAmod.Electronic.Exception.ResourseNotFoundException;
import com.CoderXAmod.Electronic.Security.JwtHelper;
import com.CoderXAmod.Electronic.Services.UserService;
import com.CoderXAmod.Electronic.dtos.JwtRequest;
import com.CoderXAmod.Electronic.dtos.JwtResponse;
import com.CoderXAmod.Electronic.dtos.UserDto;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.beans.factory.annotation.Value;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

@RestController("/auth")
@CrossOrigin("*")
public class GoogleLogin {
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private AuthenticationManager manager;
    @Autowired
    private UserService userService;
    @Autowired
    private JwtHelper helper;
    @Autowired
    private JwtHelper jwtHelper;
    @Value("${googleClientId}")
    private String googleClientId;
    @Value("${newPassword}")
    private String newPassword;
    Logger logger = LoggerFactory.getLogger(GoogleLogin.class);

    @PostMapping("/google")
    public ResponseEntity<JwtResponse> loginWithGoogle(@RequestBody Map<String, Object> data) throws IOException {
        //get Id Token from Request
        String idToken = data.get("idToken").toString();
        // ab hme google ki api ko call krni hai
        // Uske Liye hme NtHttpClass Chahiye
        NetHttpTransport netHttpTransport = new NetHttpTransport();
        JacksonFactory jacksonFactory = JacksonFactory.getDefaultInstance();
        //YE CLASS USE HOGI VARIFY BANANE K LIYE
        GoogleIdTokenVerifier.Builder verifier = new GoogleIdTokenVerifier.Builder(netHttpTransport, jacksonFactory).setAudience(Collections.singleton(googleClientId));
        GoogleIdToken googleIdToken = GoogleIdToken.parse(verifier.getJsonFactory(), idToken);
        GoogleIdToken.Payload payload = googleIdToken.getPayload();
        logger.info("Payload data is :{}", payload);
        String email = payload.getEmail();
        logger.info("user email is :{}", email);
        User user = null;
        user = userService.findUserByEmailForGoogleAuth(email).orElse(null);
        if (user == null) {
            //create new user
            user = this.saveUser(email, data.get("name").toString(), data.get("photoUrl").toString());
        }
        ResponseEntity<JwtResponse> jwtResponseResponseEntity = this.loginWithGoogle((Map<String, Object>) JwtRequest.builder().email(user.getEmail()).password(newPassword).build());
        return jwtResponseResponseEntity;
    }

    @PostMapping("/login")
    public ResponseEntity<JwtResponse> login(@RequestBody JwtRequest request) {
        this.doAuthenticate(request.getEmail(), request.getPassword());
        UserDetails userDetails = userDetailsService.loadUserByUsername(request.getEmail());
        String token = helper.generateToken(userDetails);
        UserDto userDto = mapper.map(userDetails, UserDto.class);
        JwtResponse response = JwtResponse.builder().jwtToken(token)
                .user(userDto).build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    private void doAuthenticate(String email, String password) {
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(email, password);
        try {
            manager.authenticate(authentication);
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("Invalid User Name or Password Exception !!");
        }
    }


    private User saveUser(String email, String name, String photoUrl) {
        UserDto newUser = UserDto.builder().name(name)
                .email(email)
                .password(newPassword)
                .UserImage(photoUrl)
                .roles(new HashSet<>())
                .build();
        UserDto user = userService.CreateUder(newUser);
        return this.mapper.map(user, User.class);
    }
}

package com.CoderXAmod.Electronic.reposoteries;

import com.CoderXAmod.Electronic.Entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,String> {
}

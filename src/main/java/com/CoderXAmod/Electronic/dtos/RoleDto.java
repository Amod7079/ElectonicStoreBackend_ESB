package com.CoderXAmod.Electronic.dtos;
import lombok.*;
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RoleDto {
    protected String roleId;
    protected String roleName;
}

package com.CoderXAmod.Electronic.dtos;

import lombok.*;
import org.springframework.stereotype.Component;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Component
public class JwtRequest {
    private String email;
    private String password;


}

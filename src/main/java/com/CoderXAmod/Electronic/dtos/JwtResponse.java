package com.CoderXAmod.Electronic.dtos;

import lombok.*;
import org.springframework.stereotype.Component;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Component
public class JwtResponse {
    private String jwtToken;
    private UserDto user;
}

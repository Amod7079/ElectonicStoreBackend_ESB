package com.CoderXAmod.Electronic;

import com.CoderXAmod.Electronic.Entities.Role;
import com.CoderXAmod.Electronic.reposoteries.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
@SpringBootApplication
public class ElectronicStoreApplication implements CommandLineRunner {
    @Value("${role.admin.id}")
    String role_admin;
    @Value("${role.normal.id}")
    String role_normal;
    @Autowired
    RoleRepository roleRepository;

    public static void main(String[] args)  {
        SpringApplication.run(ElectronicStoreApplication.class, args);


    }
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Override
    public void run(String... args) throws Exception {
        System.out.println("😒😒"+passwordEncoder.encode("Amod123@"));

        try {
            Role roleAdmin = Role.builder().roleId(role_admin).roleName("ROLE_ADMIN").build();
            Role roleNormal = Role.builder().roleId(role_normal).roleName("ROLE_NORMAL").build();
            roleRepository.save(roleAdmin);
            roleRepository.save(roleNormal);
        }
        catch (Exception e)
        {
e.printStackTrace();
        }
    }
}
